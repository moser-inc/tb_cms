# Change Log

## v1.2.2

- Add a `page_title` attribute to pages to allow SEO-friendly titles that may differ from the simple page name attribute.
- Snippets module is now turned off by default rather than on.

## v1.2.1

- Resolve a tinymce bug in the page editor

## v1.2.0

- Compatability with Rails 4.2, TB Core 1.3, and Bootstrap 3.

## v1.1.3

- Introduce layout actions, a simplew way to run Ruby code on a given layout
- Fix a template parsing bug releated to Rails 4.1

## v1.1.2

- Rename the `SpudPage.public` scope to `SpudPage.viewable`. This fixes a fatal error when running against Rails 4.1.1

## v1.1.1

- Squash bugs

## v1.1

- Support for Rails 4 and tb_core 1.2
- Fragment caching for pages and `sp_list_menu` helper

## v1.0.2

- Use the new Spud::NotFoundError for 404 handling in pages controller
