require 'rails_helper'

describe SpudPagePartial, type: :model do
  describe 'validations' do

    it 'should require a name' do
      p = FactoryBot.build(:spud_page_partial, name: nil)
      expect(p).to_not be_valid
    end

    it 'should respond with a symbol_name based on name' do
      p = FactoryBot.build(:spud_page_partial, name: 'Test Page')
      expect(p.symbol_name).to eq('test_page')
    end
  end

  describe 'save hooks' do
    it 'should save the symbol name' do
      p = FactoryBot.create(:spud_page_partial, name: 'Test Page')
      expect(p.attributes['symbol_name']).to eq('test_page')
    end

    it 'should create a new revision if content is changed' do
      p = FactoryBot.create(:spud_page_partial, name: 'Test Page', content: 'Content')
      expect(SpudPagePartialRevision.where(spud_page_id: p.spud_page_id).count).to eq(1)
    end

    it 'should delete old revisions beyond max_revision count' do
      Spud::Cms.configure do |config|
        config.max_revisions = 2
      end
      p = FactoryBot.create(:spud_page_partial, name: 'Test Page', content: 'Home Sweet Home', spud_page_id: 1)
      p.content = 'Nah'
      p.save

      p.content = 'Another change'
      p.save
      expect(SpudPagePartialRevision.where(spud_page_id: 1, name: 'Test Page').count).to eq(2)
    end

  end

end
