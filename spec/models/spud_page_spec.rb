require 'rails_helper'

describe SpudPage, type: :model do

  it { should have_many(:spud_page_partials) }
  it { should have_many(:spud_pages) }
  it { should belong_to(:spud_page) }
  it { should belong_to(:created_by_user) }
  it { should belong_to(:updated_by_user) }

  describe 'validations' do

    it 'should require a name' do
      p = FactoryBot.build(:spud_page, name: nil)
      expect(p).to_not be_valid
    end

    it 'should require a unique url_name' do
      FactoryBot.create(:spud_page, url_name: 'test', use_custom_url_name: true)
      t = FactoryBot.build(:spud_page, url_name: 'test', use_custom_url_name: true)
      expect(t).to_not be_valid
    end

    it 'should generate a url_name if taken' do
      FactoryBot.create(:spud_page, name: 'test')
      t = FactoryBot.build(:spud_page, name: 'test')
      expect do
        t.valid?
      end.to change(t, :url_name)
    end

    it 'should dependantly destroy page_partials' do
      t = FactoryBot.create(:spud_page, spud_page_partials: [SpudPagePartial.new(name: 'body')])
      expect do
        t.destroy
      end.to change(SpudPagePartial, :count).from(1).to(0)
    end

  end

  describe 'scopes' do

    it 'should only show published pages' do
      expect(SpudPage.published_pages.to_sql).to eq(SpudPage.where(published: true).to_sql)
    end

    it 'should only show parent pages' do
      expect(SpudPage.parent_pages.to_sql).to eq(SpudPage.where(spud_page_id: nil).to_sql)
    end

    it 'should only show public pages' do
      expect(SpudPage.viewable.to_sql).to eq(SpudPage.where(visibility: 0).to_sql)
    end

    it 'should return private if visibility is == 1' do
      parent_page = FactoryBot.build(:spud_page, name: 'parent', visibility: 1)

      expect(parent_page.is_private?).to eq(true)

      parent_page.visibility = 0
      expect(parent_page.is_private?).to eq(false)
    end
  end

  describe '#generate_url_name' do
    it 'should use the provided url' do
      @page = FactoryBot.create(:spud_page, url_name: 'test')
      expect(@page.url_name).to eq('test')
    end

    it 'should generate a url based on the page name' do
      @page = FactoryBot.create(:spud_page, name: 'Hello World')
      expect(@page.url_name).to eq('hello-world')
    end

    it 'should generate a url based on the page name and parent page' do
      @parent = FactoryBot.create(:spud_page, name: 'About Us')
      @page = FactoryBot.create(:spud_page, name: 'Hello World', spud_page: @parent)
      expect(@page.url_name).to eq('about-us/hello-world')
    end
  end

  describe '#create_redirect_if_necessary' do
    it 'should create a redirect' do
      page = FactoryBot.create(:spud_page, url_name: 'page-a')
      expect do
        page.update(url_name: 'page-b')
      end.to change(page.tb_redirects, :count).by(1)
      redirect = page.tb_redirects.last
      expect(redirect.source).to eq('/page-a')
      expect(redirect.destination).to eq('/page-b')
    end
  end

  describe '.find_ignoring_case' do
    it 'finds the page ignoring case' do
      page = FactoryBot.create(:spud_page, url_name: 'HEy-guyS-lETS-Mix-CAsE')
      found = SpudPage.find_ignoring_case('hey-guys-lets-mix-case')
      expect(page).to eq(found)
    end
  end

end
