require 'rails_helper'

describe Cms::ApplicationHelper, type: :helper do
  before(:each) do
    helper.output_buffer = ''
  end

  describe '#sp_list_menu' do
    it 'should be able to find a menu by its name' do
      menu = FactoryBot.create(:spud_menu, name: 'Main')
      2.times { |_x| FactoryBot.create(:spud_menu_item, parent_id: menu.id, spud_menu_id: menu.id, url: '/') }

      result = helper.sp_list_menu(name: 'Main', active_class: 'active')
      expect(result).to match /\<li/
    end

    it 'should assign id to ul block' do
      menu = FactoryBot.create(:spud_menu)
      2.times { |_x| s = FactoryBot.create(:spud_menu_item, parent_id: menu.id, spud_menu_id: menu.id, url: '/') }

      result = helper.sp_list_menu(name: menu.name, id: 'nav')
      expect(result).to match /id=\'nav\'/
    end

    it 'should render nested menu items' do
      menu = FactoryBot.create(:spud_menu, name: 'Main2')
      s = FactoryBot.create(:spud_menu_item, parent_id: menu.id, spud_menu_id: menu.id, url: '/')
      s2 = FactoryBot.create(:spud_menu_item, parent_id: menu.id, spud_menu_id: menu.id, url: '/')
      s3 = FactoryBot.create(:spud_menu_item, parent_type: 'SpudMenuItem', parent_id: s.id, spud_menu_id: menu.id, url: '/', name: 'SubItem')

      result = helper.sp_list_menu(name: 'Main2')
      expect(result).to match /SubItem/
    end

    it 'should respect max depth' do
      menu = FactoryBot.create(:spud_menu, name: 'Main4')
      s = FactoryBot.create(:spud_menu_item, parent_id: menu.id, spud_menu_id: menu.id, url: '/')
      s2 = FactoryBot.create(:spud_menu_item, parent_id: menu.id, spud_menu_id: menu.id, url: '/')
      s3 = FactoryBot.create(:spud_menu_item, parent_type: 'SpudMenuItem', parent_id: s.id, spud_menu_id: menu.id, url: '/', name: 'SubItem')

      result = helper.sp_list_menu(name: 'Main4', max_depth: 1)
      expect(result).to_not match /SubItem/
    end
  end

  describe '#sp_menu_with_seperator' do
    it 'should render a flattened list of links' do
      menu = FactoryBot.create(:spud_menu, name: 'Main3')
      s = FactoryBot.create(:spud_menu_item, parent_id: menu.id, spud_menu_id: menu.id, url: '/')
      s2 = FactoryBot.create(:spud_menu_item, parent_id: menu.id, spud_menu_id: menu.id, url: '/')
      s3 = FactoryBot.create(:spud_menu_item, parent_type: 'SpudMenuItem', parent_id: s.id, spud_menu_id: menu.id, url: '/', name: 'SubItem')

      content = helper.sp_menu_with_seperator(name: 'Main3')
      expect(content).to match /SubItem/
      expect(content).to_not match /\<li/
    end
  end

  describe '#sp_list_pages' do
    it 'should be able to list created pages' do
      page = FactoryBot.create(:spud_page)
      page2 = FactoryBot.create(:spud_page)
      page3 = FactoryBot.create(:spud_page, spud_page_id: page.id)

      content = helper.sp_list_pages(active_class: 'active')
      expect(content).to match /#{page.name}/
      expect(content).to match /#{page2.name}/
      expect(content).to match /#{page3.name}/
    end

    it 'should assign id' do
      page = FactoryBot.create(:spud_page)
      page2 = FactoryBot.create(:spud_page)

      content = helper.sp_list_pages(id: 'page_nav')
      expect(content).to match /id=\'page_nav\'/

    end

    it 'should be able to exclude certain pages' do
      page = FactoryBot.create(:spud_page)
      page2 = FactoryBot.create(:spud_page)

      content = helper.sp_list_pages(exclude: [page2.name])
      expect(content).to match /#{page.name}/
      expect(content).to_not match /#{page2.name}/
    end
    it 'should respect max depth' do
      page = FactoryBot.create(:spud_page)
      page2 = FactoryBot.create(:spud_page)
      page3 = FactoryBot.create(:spud_page, spud_page_id: page.id)

      content = helper.sp_list_pages(max_depth: 1)
      expect(content).to match /#{page.name}/
      expect(content).to match /#{page2.name}/
      expect(content).to_not match /#{page3.name}/
    end

    it 'should be able to list sub pages of a particular page' do
      page = FactoryBot.create(:spud_page)
      page2 = FactoryBot.create(:spud_page)
      page3 = FactoryBot.create(:spud_page, spud_page_id: page.id)
      content = helper.sp_list_pages(start_page_id: page.id)
      expect(content).to_not match /#{page.name}/
      expect(content).to_not match /#{page2.name}/
      expect(content).to match /#{page3.name}/
    end
  end
end
