require 'rails_helper'

describe Admin::MenuItemsController, type: :controller do
  before(:each) do
    activate_session(admin: true)
  end

  let (:menu) do
    FactoryBot.create(:spud_menu)
  end

  describe 'index' do
    it 'should return an array of menus' do
      2.times { |_x| s = FactoryBot.create(:spud_menu_item, spud_menu_id: menu.id, parent_id: menu.id) }
      get :index, params: { menu_id: menu.id }

      expect(assigns(:menu_items).count).to be > 1
    end
    it 'should redirect if the menu is not found' do
      get :index, params: { menu_id: menu.id + 1 }
      expect(response).to redirect_to admin_menus_url
    end
  end

  describe 'new' do
    it 'should assign a new menu' do
      get :new, params: { menu_id: menu.id }
      expect(assigns(:menu_item)).to_not be_blank
      expect(response).to be_success
    end
  end

  describe 'create' do
    it 'should create a new menu item with a valid form submission' do
      expect do
        post :create, params: { menu_id: menu.id, spud_menu_item: FactoryBot.attributes_for(:spud_menu_item, parent_id: nil).reject { |k, _v| k == 'id' || k == :spud_menu_id } }
      end.to change(SpudMenuItem, :count).by(1)
      expect(response).to be_redirect
    end

    it 'should not create a menu item with an invalid form entry' do
      yamldata = FactoryBot.attributes_for(:spud_menu_item, name: nil).reject { |k, _v| k == 'id' || k == :spud_menu_id }

      expect do
        post :create, params: { menu_id: menu.id, spud_menu_item: FactoryBot.attributes_for(:spud_menu_item, name: nil, parent_id: nil).reject { |k, _v| k == 'id' || k == :spud_menu_id } }
      end.to_not change(SpudMenuItem, :count)
    end

    it 'should autoset order if not specified' do
      menuitem = FactoryBot.create(:spud_menu_item, parent_id: menu.id)

      post :create, params: { menu_id: menu.id, spud_menu_item: FactoryBot.attributes_for(:spud_menu_item, menu_order: nil, parent_id: nil).reject { |k, _v| k == 'id' || k == :spud_menu_id } }
      expect(assigns(:menu_item).menu_order).to eq(1)
    end

    it 'should assign the menu name based on page name if left blank' do
      page = FactoryBot.create(:spud_page)
      post :create, params: { menu_id: menu.id, spud_menu_item: FactoryBot.attributes_for(:spud_menu_item, name: nil, spud_page_id: page.id).reject { |k, _v| k == 'id' || k == :spud_menu_id } }
      expect(assigns(:menu_item).name).to eq(page.name)
    end

    it 'should set the parent to a submenu if specified' do
      menuitem = FactoryBot.create(:spud_menu_item, parent_id: menu.id)
      post :create, params: { menu_id: menu.id, spud_menu_item: FactoryBot.attributes_for(:spud_menu_item, parent_id: menuitem.id).reject { |k, _v| k == 'id' || k == :spud_menu_id } }
      expect(assigns(:menu_item).parent).to eq(menuitem)
    end
  end

  describe 'edit' do
    it 'should respond with menu item by id' do
      menu1 = FactoryBot.create(:spud_menu_item)
      menu2 = FactoryBot.create(:spud_menu_item)
      get :edit, params: { menu_id: menu.id, id: menu2.id }
      expect(assigns(:menu_item)).to eq(menu2)
    end

    it 'should redirect_to index if menu item not found' do
      get :edit, params: { menu_id: menu.id, id: '345' }
      expect(response).to redirect_to admin_menu_menu_items_url(menu_id: menu.id)
    end
  end

  describe 'update' do
    it 'should update the name when the name attribute is changed' do
      menu_item = FactoryBot.create(:spud_menu_item)
      new_name = 'MyMenu'
      expect do
        put :update, params: { menu_id: menu.id, id: menu_item.id, spud_menu_item: menu_item.attributes.merge!(name: new_name, parent_id: nil).reject { |k, _v| k.to_sym == :spud_menu_id || k == 'id' || k.to_sym == :updated_at || k.to_sym == :created_at } }
        menu_item.reload
      end.to change(menu_item, :name).to(new_name)
    end
    it 'should assign a menu to a submenu if parentid is defined' do
      menu_item1 = FactoryBot.create(:spud_menu_item)
      menu_item = FactoryBot.create(:spud_menu_item)

      expect do
        put :update, params: { menu_id: menu.id, id: menu_item.id, spud_menu_item: menu_item.attributes.merge!(parent_id: menu_item1.id).reject { |k, _v| k.to_sym == :spud_menu_id || k == 'id' || k.to_sym == :updated_at || k.to_sym == :created_at } }
        menu_item.reload
      end.to change(menu_item, :parent).to(menu_item1)
    end
  end

  describe 'destroy' do
    it 'should destroy the menu item' do
      menu_item = FactoryBot.create(:spud_menu_item, parent_id: menu.id)
      expect do
        delete :destroy, params: { menu_id: menu.id, id: menu_item.id }
      end.to change(SpudMenuItem, :count).by(-1)
    end
  end

  describe 'update_sort' do
    it 'should update all menu items menu_order fields' do
      ordered_arr = Array.new(5)
      ordered_arr.each_with_index do |item, index|
        item = FactoryBot.create(:spud_menu_item, parent_id: menu.id, menu_order: index)
        ordered_arr[index] = item.id.to_s
      end
      new_order_arr = ordered_arr.shuffle
      put :update_sort, params: { menu_id: menu.id, id: ordered_arr[0], order: new_order_arr }
      new_order_arr.each_with_index do |item, index|
        expect(menu.spud_menu_items.find_by(id: item.to_i).menu_order).to be(index)
      end
    end
  end
end
