require 'rails_helper'

describe Admin::PagesController, type: :controller do
  before(:each) do
    activate_session(admin: true)
  end

  describe 'index' do
    it 'should return an array of parent pages' do
      2.times { |_x| s = FactoryBot.create(:spud_page) }
      get :index

      expect(assigns(:pages).count).to be > 1
    end

    it 'should only return parent pages' do
      2.times { |_x| FactoryBot.create(:spud_page) }
      FactoryBot.create(:spud_page, spud_page_id: 1)

      get :index
      expect(assigns(:pages).count).to eq(2)
    end
  end

  describe 'show' do
    it 'should respond successfully' do
      p = FactoryBot.create(:spud_page)
      get :show, params: { id: p.id }
      expect(assigns(:layout)).to render_template(layout: "layouts/#{Spud::Cms.default_page_layout}")
      expect(response).to be_success
    end
  end

  describe 'new' do
    it 'should respond successfully' do
      get :new

      expect(response).to be_success
    end

    it 'should build a page object for the form' do
      get :new

      expect(assigns(:page)).to_not be_blank
    end
  end

  describe 'edit' do
    context 'HTML format' do
      it 'should load the correct page for the edit form' do
        page = FactoryBot.create(:spud_page)
        get :edit, params: { id: page.id }

        expect(assigns(:page).id).to eq(page.id)
      end
    end

  end

  describe 'update' do
    it 'should update the name when the attribute is changed' do
      page = FactoryBot.create(:spud_page)
      new_name = 'Adam'
      expect do
        put :update, params: { id: page.id, spud_page: { name: new_name } }
        page.reload
      end.to change(page, :name).to(new_name)
    end

    it 'should redirect to the page root index after a successful update' do
      page = FactoryBot.create(:spud_page)
      new_name = 'Adam'
      put :update, params: { id: page.id, spud_page: page.attributes.merge!(name: new_name).reject { |key, _value| key == 'id' || key == 'created_at' || key == 'updated_at' } }

      expect(response).to redirect_to(admin_pages_url)
    end
  end

  describe 'destroy' do
    it 'should destroy the page' do
      page = FactoryBot.create(:spud_page)
      expect do
        delete :destroy, params: { id: page.id }
      end.to change(SpudPage, :count).by(-1)
      expect(response).to be_redirect
    end

    it 'should destroy the user with the wrong id' do
      page = FactoryBot.create(:spud_page)
      expect do
        delete :destroy, params: { id: '23532' }
      end.to_not change(SpudPage, :count)
      expect(response).to be_redirect
    end
  end

end
