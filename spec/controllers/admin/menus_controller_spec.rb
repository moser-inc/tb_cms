require 'rails_helper'

describe Admin::MenusController, type: :controller do
  before(:each) do
    activate_session(admin: true)
  end

  describe 'index' do
    it 'should return an array of menus' do
      2.times { FactoryBot.create(:spud_menu) }
      get :index

      expect(assigns(:menus).count).to be > 1
    end

  end

  describe 'new' do
    it 'should response with new menu' do
      get :new
      expect(assigns(:menu)).to_not be_blank
      expect(response).to be_success
    end
  end

  describe 'create' do
    it 'should create a new menu with a valid form submission' do
      expect do
        post :create, params: { spud_menu: FactoryBot.attributes_for(:spud_menu).reject { |k, _v| k == 'id' } }
      end.to change(SpudMenu, :count).by(1)
      expect(response).to be_redirect
    end

    it 'should not create a menu with an invalid form entry' do
      expect do
        post :create, params: { spud_menu: FactoryBot.attributes_for(:spud_menu, name: nil).reject { |k, _v| k == 'id' } }
      end.to_not change(SpudMenu, :count)

    end
  end

  describe 'edit' do
    it 'should response with menu by id' do
      menu1 = FactoryBot.create(:spud_menu)
      menu2 = FactoryBot.create(:spud_menu)
      get :edit, params: { id: menu2.id }
      expect(assigns(:menu)).to eq(menu2)
    end
    it 'should redirect to index if menu not found' do
      get :edit, params: { id: 3 }
      expect(response).to redirect_to admin_menus_url
    end

  end

  describe 'update' do
    it 'should update the name when the name attribute is changed' do
      menu = FactoryBot.create(:spud_menu)
      new_name = 'MyMenu'
      expect do
        put :update, params: { id: menu.id, spud_menu: menu.attributes.merge!(name: new_name).reject { |k, _v| k == 'id' } }
        menu.reload
      end.to change(menu, :name).to(new_name)

    end

    it 'should redirect to the admin menus after a successful update' do
      menu = FactoryBot.create(:spud_menu)
      put :update, params: { id: menu.id, spud_menu: menu.attributes.merge!(name: 'MyMenu').reject { |k, _v| k == 'id' } }

      expect(response).to redirect_to(admin_menu_menu_items_url(menu_id: menu.id))
    end
  end

  describe 'destroy' do
    it 'should destroy the menu' do
      menu = FactoryBot.create(:spud_menu)
      expect do
        delete :destroy, params: { id: menu.id }
      end.to change(SpudMenu, :count).by(-1)
      expect(response).to be_redirect
    end

    it 'should not destroy the menu with a wrong id' do
      menu = FactoryBot.create(:spud_menu)
      expect do
        delete :destroy, params: { id: '23532' }
      end.to_not change(SpudMenu, :count)
      expect(response).to be_redirect
    end

  end
end
