require 'rails_helper'

describe PagesController, type: :controller do
  describe 'show' do

    it 'should render a page' do
      page = FactoryBot.create(:spud_page)
      get :show, params: { id: page.url_name }
      expect(assigns(:layout)).to render_template(layout: "layouts/#{Spud::Cms.default_page_layout}")
      expect(response).to be_success
    end

    it 'should render home page if id is blank' do
      page = FactoryBot.create(:spud_page, name: 'home')
      get :show
      expect(assigns(:page)).to eq(page)
      expect(response).to be_success
    end

    it 'should redirect to new page url from old page url if it was changed' do
      page = FactoryBot.create(:spud_page, name: 'about', url_name: 'about')
      page.update(url_name: 'about-us')
      page.save
      get :show, params: { id: 'about' }

      expect(response).to redirect_to page_url(id: 'about-us')
    end

    it 'should not allow access to private pages when logged out' do
      page = FactoryBot.create(:spud_page, name: 'about', visibility: 1)

      get :show, params: { id: 'about' }
      expect(response).to redirect_to login_path(return_to: '/about')
    end

    describe 'authorized login' do
      before(:each) do
        activate_authlogic
        u = SpudUser.new(login: 'testuser', email: 'test@testuser.com', password: 'test', password_confirmation: 'test')
        u.super_admin = true
        u.save
        @user = SpudUserSession.create(u)
      end

      it 'should allow access to private pages when logged in' do
        page = FactoryBot.create(:spud_page, name: 'about', visibility: 1)

        get :show, params: { id: 'about' }
        expect(response).to be_success
      end
    end

  end
end
