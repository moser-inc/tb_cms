# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160215180157) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "spud_menu_items", id: :serial, force: :cascade do |t|
    t.string "parent_type"
    t.integer "parent_id"
    t.integer "item_type"
    t.integer "spud_page_id"
    t.integer "menu_order", default: 0
    t.string "url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "name"
    t.integer "spud_menu_id"
    t.string "classes"
    t.index ["menu_order"], name: "index_spud_menu_items_on_menu_order"
    t.index ["parent_type", "parent_id"], name: "index_spud_menu_items_on_parent_type_and_parent_id"
    t.index ["spud_menu_id"], name: "index_spud_menu_items_on_spud_menu_id"
  end

  create_table "spud_menus", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "spud_page_partial_revisions", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "content"
    t.string "format"
    t.integer "spud_page_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["spud_page_id", "name"], name: "revision_idx"
  end

  create_table "spud_page_partials", id: :serial, force: :cascade do |t|
    t.integer "spud_page_id"
    t.string "name"
    t.text "content"
    t.string "format"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "symbol_name"
    t.text "content_processed"
    t.index ["spud_page_id"], name: "index_spud_page_partials_on_spud_page_id"
  end

  create_table "spud_pages", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "url_name"
    t.datetime "publish_at"
    t.integer "created_by"
    t.integer "updated_by"
    t.string "format", default: "html"
    t.integer "spud_page_id"
    t.text "meta_description"
    t.string "meta_keywords"
    t.integer "page_order"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "visibility", default: 0
    t.boolean "published", default: true
    t.boolean "use_custom_url_name", default: false
    t.text "notes"
    t.string "layout"
    t.string "page_title"
  end

  create_table "spud_permalinks", id: :serial, force: :cascade do |t|
    t.string "url_name"
    t.string "attachment_type"
    t.integer "attachment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "site_id", default: 0, null: false
    t.index ["attachment_type", "attachment_id"], name: "idx_permalink_attachment"
    t.index ["site_id"], name: "idx_permalinks_site_id"
  end

  create_table "spud_permissions", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.string "tag", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["tag"], name: "index_spud_permissions_on_tag", unique: true
  end

  create_table "spud_role_permissions", id: :serial, force: :cascade do |t|
    t.integer "spud_role_id", null: false
    t.string "spud_permission_tag", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["spud_permission_tag"], name: "index_spud_role_permissions_on_spud_permission_tag"
    t.index ["spud_role_id"], name: "index_spud_role_permissions_on_spud_role_id"
  end

  create_table "spud_roles", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "spud_snippets", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "content"
    t.string "format"
    t.text "content_processed"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["name"], name: "index_spud_snippets_on_name"
  end

  create_table "spud_user_settings", id: :serial, force: :cascade do |t|
    t.integer "spud_user_id"
    t.string "key"
    t.string "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "spud_users", id: :serial, force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.boolean "super_admin"
    t.string "login", null: false
    t.string "email", null: false
    t.string "crypted_password", null: false
    t.string "password_salt", null: false
    t.string "persistence_token", null: false
    t.string "single_access_token", null: false
    t.string "perishable_token", null: false
    t.integer "login_count", default: 0, null: false
    t.integer "failed_login_count", default: 0, null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string "current_login_ip"
    t.string "last_login_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "time_zone"
    t.integer "spud_role_id"
    t.boolean "requires_password_change", default: false
    t.index ["email"], name: "index_spud_users_on_email"
    t.index ["login"], name: "index_spud_users_on_login"
    t.index ["spud_role_id"], name: "index_spud_users_on_spud_role_id"
  end

  create_table "tb_redirects", id: :serial, force: :cascade do |t|
    t.string "owner_type"
    t.integer "owner_id"
    t.string "source", null: false
    t.string "destination", null: false
    t.string "created_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_type", "owner_id"], name: "index_tb_redirects_on_owner_type_and_owner_id"
    t.index ["source"], name: "index_tb_redirects_on_source", unique: true
  end

end
