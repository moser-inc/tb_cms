# This migration comes from tb_cms (originally 20120104194032)
class AddVisibilityToSpudPages < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_pages, :visibility, :integer, default: 0
    add_column :spud_pages, :published, :boolean, default: true
  end
end
