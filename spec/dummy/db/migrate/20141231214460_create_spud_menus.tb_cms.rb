# This migration comes from tb_cms (originally 20120101193138)
class CreateSpudMenus < ActiveRecord::Migration[4.2]
  def change
    create_table :spud_menus do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
