# This migration comes from tb_cms (originally 20120107181337)
class AddMenuNameToSpudMenuItems < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_menu_items, :name, :string
  end
end
