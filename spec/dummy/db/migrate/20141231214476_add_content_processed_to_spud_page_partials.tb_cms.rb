# This migration comes from tb_cms (originally 20121016233715)
class AddContentProcessedToSpudPagePartials < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_page_partials, :content_processed, :text
  end
end
