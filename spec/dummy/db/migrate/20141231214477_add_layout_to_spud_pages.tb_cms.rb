# This migration comes from tb_cms (originally 20121112151110)
class AddLayoutToSpudPages < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_pages, :layout, :string

    remove_column :spud_pages, :template_id
    drop_table :spud_templates
  end
end
