# This migration comes from tb_cms (originally 20120510195151)
class CreateSpudPagePartialRevisions < ActiveRecord::Migration[4.2]
  def change
    create_table :spud_page_partial_revisions do |t|
      t.string :name
      t.text :content
      t.string :format
      t.integer :spud_page_id
      t.timestamps
    end

    add_index :spud_page_partial_revisions, [:spud_page_id, :name], name: 'revision_idx'
  end
end
