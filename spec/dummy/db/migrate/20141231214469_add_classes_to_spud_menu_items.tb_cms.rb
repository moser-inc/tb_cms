# This migration comes from tb_cms (originally 20120128163601)
class AddClassesToSpudMenuItems < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_menu_items, :classes, :string
  end
end
