# This migration comes from tb_permalinks (originally 20120329135522)
class AddSiteIdToSpudPermalinks < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_permalinks, :site_id, :integer
    add_index :spud_permalinks, :site_id, name: 'idx_permalinks_site_id'
  end
end
