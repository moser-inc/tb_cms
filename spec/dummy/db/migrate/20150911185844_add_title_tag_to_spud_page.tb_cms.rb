# This migration comes from tb_cms (originally 20150622161403)
class AddTitleTagToSpudPage < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_pages, :page_title, :string
  end
end
