module CmsDeprecatedMultisite
  extend ActiveSupport::Concern

  included do
    scope :site, ->(_sid) {
      ActiveSupport::Deprecation.warn 'Model.site scope is deprecated and will be removed in the future'
      all()
    }
  end

  def site_id
    ActiveSupport::Deprecation.warn '#site_id is deprecated and will be removed in the future'
    return 0
  end

  def site_id=(_id)
    ActiveSupport::Deprecation.warn '#site_id is deprecated and will be removed in the future'
    return nil
  end

end
