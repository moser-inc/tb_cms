class SpudSnippet < ActiveRecord::Base
  include CmsDeprecatedMultisite

  validates :name, presence: true
  validates :name, uniqueness: true

  def postprocess_content
    content
  end

  def content_processed=(content)
    self[:content_processed] = content
  end

  def content_processed
    if self[:content_processed].blank?
      update_column(:content_processed, postprocess_content)
    end
    return self[:content_processed]
  end

end
