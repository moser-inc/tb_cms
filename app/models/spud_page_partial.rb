class SpudPagePartial < ActiveRecord::Base
  belongs_to :spud_page, touch: true
  validates :name, presence: true
  before_save :maintain_revisions
  before_save :update_symbol_name

  def update_symbol_name
    self.symbol_name = name.parameterize.underscore
  end

  def symbol_name
    return @symbol_name || name.parameterize.underscore
  end

  def content_processed
    ActiveSupport::Deprecation.warn('#content_processed is deprecated; use #content instead.')
    content
  end

  def maintain_revisions
    return true unless changed.include?('content')
    revision = SpudPagePartialRevision.new(spud_page_id: spud_page_id, name: name, format: format, content: content)
    revision.save
    if Spud::Cms.max_revisions > 0
      revision_count = SpudPagePartialRevision.where(spud_page_id: spud_page_id, name: name).count
      if revision_count > Spud::Cms.max_revisions
        revision_bye = SpudPagePartialRevision.where(spud_page_id: spud_page_id, name: name).order('created_at ASC').first
        revision_bye.destroy unless revision_bye.blank?
      end
    end
    return true
  end
end
