class Cms::SitemapsController < TbCore::ApplicationController
  respond_to :xml
  def show
    @pages = SpudPage.published_pages.viewable.order(:spud_page_id)
    respond_with @pages
  end
end
