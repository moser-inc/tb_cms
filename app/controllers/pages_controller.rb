class PagesController < ApplicationController
  include SpudCmsLayoutActions

  respond_to :html
  before_action :set_default_content_type

  def show
    # prevents 500 errors if a url like "/home.jpg" is hit
    if request.format != :html
      render_404
      return
    end

    url_name = !params[:id].blank? ? params[:id] : Spud::Cms.root_page_name
    @page = SpudPage.published_pages.find_ignoring_case(url_name)

    if @page.blank?
      render_404
      return
    end

    if @page.is_private?
      return if defined?(require_user) && require_user == false
    end

    layout = @page.layout || Spud::Cms.default_page_layout

    action_name = "#{layout}_action"
    send(action_name, request.method.downcase.to_sym) if respond_to?(action_name)

    render layout: layout
  end

  private

  def render_404
    raise TbCore::NotFoundError, item: 'page'
  end

  def set_default_content_type
    request.format = :html if params[:format].blank?
  end

end
