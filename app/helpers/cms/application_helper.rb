module Cms::ApplicationHelper
  MENU_INDEX = {}.freeze

  def sp_snippet(name, snippets = nil)
    return '' if name.blank?
    snippet = if !snippets.blank?
                snippets.select { |s| s.name == name }
              else
                SpudSnippet.where(name: name).first
              end
    if !snippet.blank?
      # cache(snippet) do
      # 	concat snippet.content_processed.html_safe
      # end
      snippet.content_processed.html_safe
    else
      return nil
    end
  end

  def sp_list_pages(options = {})
    pages = SpudPage.viewable.published_pages

    start_page = nil
    max_depth = 0
    active_class = 'menu-active'

    if !options.blank?
      if options.key?(:exclude)

        pages = pages.where(['name NOT IN (?)', options[:exclude]])
      end
      start_page = options[:start_page_id] if options.key?(:start_page_id)
      content = if options.key?(:id)
                  "<ul id='#{options[:id]}' #{"class='#{options[:class]}'" if options.key?(:class)}>"
                else
                  "<ul #{"class='#{options[:class]}'" if options.key?(:class)}>"
                end
      active_class = options[:acive_class] if options.key?(:active_class)
      max_depth = options[:max_depth] if options.key?(:max_depth)

    else
      content = '<ul>'
    end

    pages = pages.to_a.group_by(&:spud_page_id)
    return '' if pages[start_page].blank?
    pages[start_page].sort_by(&:page_order).each do |page|
      active = false
      unless page.url_name.blank?
        if current_page?(page_path(id: page.url_name))
          active = true
        elsif page.url_name == Spud::Cms.root_page_name && current_page?(root_path)
          active = true
        end
      end
      content += "<li class='#{active_class if active}'><a href='#{page_path(id: page.url_name)}'>#{page.name}</a>"
      if max_depth == 0 || max_depth > 1
        content += sp_list_page(page, pages, 2, max_depth, options)
      end
      content += '</li>'
    end
    content += '</ul>'

    return content.html_safe
  end

  def sp_list_menu(options = {})
    options[:path] = request.original_fullpath

    unless options.key?(:name)
      logger.debug 'sp_list_menu require a :name option'
      return ''
    end

    max_depth = 0
    menu_id = nil
    link_options = {}
    start_menu_item = nil
    menu = SpudMenu.where(name: options[:name]).first

    if menu.blank?
      return ''
    else
      menu_id = menu.id
    end

    Rails.cache.fetch(['sp_list_menu', menu, options]) do
      link_options = options[:link_options] if options.has_key?(:link_options)

      start_menu_item = options[:start_menu_item_id] if options.key?(:start_menu_item_id)
      content = if options.key?(:id)
                  "<ul id='#{options[:id]}' #{"class='#{options[:class]}'" if options.key?(:class)}>"
                else
                  "<ul #{"class='#{options[:class]}'" if options.key?(:class)}>"
                end
      max_depth = options[:max_depth] if options.key?(:max_depth)

      menu_items = SpudMenuItem.where(spud_menu_id: menu_id).select("
        #{SpudMenuItem.table_name}.id as id,
        #{SpudMenuItem.table_name}.url as url,
        #{SpudMenuItem.table_name}.classes as classes,
        #{SpudMenuItem.table_name}.parent_type as parent_type,
        #{SpudMenuItem.table_name}.menu_order as menu_order,
        #{SpudMenuItem.table_name}.parent_id as parent_id,
        #{SpudMenuItem.table_name}.name as name,
        #{SpudPage.table_name}.url_name as url_name").order(:parent_type, :parent_id).joins("LEFT JOIN #{SpudPage.table_name} ON (#{SpudPage.table_name}.id = #{SpudMenuItem.table_name}.spud_page_id)").to_a

      grouped_items = menu_items.group_by(&:parent_type)

      return '' if grouped_items['SpudMenu'].blank?

      child_items = grouped_items['SpudMenuItem'].blank? ? [] : grouped_items['SpudMenuItem'].group_by(&:parent_id)

      parent_items = grouped_items['SpudMenu']
      parent_items = child_items[start_menu_item] unless start_menu_item.nil?

      parent_items.sort_by(&:menu_order).each do |item|
        active = false
        if !item.url_name.blank?
          if current_page?(page_path(id: item.url_name))
            active = true
          elsif item.url_name == Spud::Cms.root_page_name && current_page?(root_path)
            active = true
          end
        elsif current_page?(item.url)
          active = true
        end
        link_tag = link_to item.name, !item.url_name.blank? ? (item.url_name == Spud::Cms.root_page_name ? root_path() : page_path(id: item.url_name)) : item.url, { class: "#{'menu-active' if active} #{item.classes unless item.classes.blank?}" }.merge(link_options)
        content += "<li class='#{'menu-active' if active} #{item.classes unless item.classes.blank?}'>#{link_tag}"
        if max_depth == 0 || max_depth > 1
          content += sp_list_menu_item(child_items, item.id, 2, max_depth, options)
        end
        content += '</li>'
      end

      content += '</ul>'

      content
    end.html_safe
  end

  def sp_menu_with_seperator(options = {})
    seperator = '&nbsp;|&nbsp;'.html_safe
    seperator = options[:seperator] if options.key?(:seperator)

    menu = SpudMenu.find_by(name: options[:name])
    return '' if menu.blank?
    menu_items = menu.spud_menu_items_combined.select("
			#{SpudMenuItem.table_name}.id as id,
			#{SpudMenuItem.table_name}.url as url,
			#{SpudMenuItem.table_name}.classes as classes,
			#{SpudMenuItem.table_name}.parent_type as parent_type,
			#{SpudMenuItem.table_name}.menu_order as menu_order,
			#{SpudMenuItem.table_name}.parent_id as parent_id,
			#{SpudMenuItem.table_name}.name as name,
			#{SpudPage.table_name}.url_name as url_name").order(:parent_type, :parent_id).joins("LEFT JOIN #{SpudPage.table_name} ON (#{SpudPage.table_name}.id = #{SpudMenuItem.table_name}.spud_page_id)").to_a

    menu_tags = []
    menu_items.sort_by(&:menu_order).each do |item|
      menu_tags += ["<a #{"class='#{item.classes}' " unless item.classes.blank?}href='#{!item.url_name.blank? ? (item.url_name == Spud::Cms.root_page_name ? root_path() : page_path(id: item.url_name)) : item.url}'>#{item.name}</a>"]
    end

    return menu_tags.join(seperator).html_safe
  end

  private

  def sp_list_menu_item(items, item_id, depth, max_depth, options = {})
    link_options = options.key?(:link_options) ? options[:link_options] : {}
    spud_menu_items = items[item_id]
    return '' if spud_menu_items.nil?
    content = '<ul>'

    spud_menu_items.sort_by(&:menu_order).each do |item|
      active = false
      if !item.url_name.blank?
        if current_page?(page_path(id: item.url_name))
          active = true
        elsif item.url_name == Spud::Cms.root_page_name && current_page?(root_path)
          active = true
        end
      elsif current_page?(item.url)
        active = true
      end
      link_tag = link_to item.name, !item.url_name.blank? ? (item.url_name == Spud::Cms.root_page_name ? root_path() : page_path(id: item.url_name)) : item.url, { class: "#{'menu-active' if active} #{item.classes unless item.classes.blank?}" }.merge(link_options)
      content += "<li class='#{'menu-active' if active} #{item.classes unless item.classes.blank?}'>#{link_tag}"
      if max_depth == 0 || max_depth > depth
        content += sp_list_menu_item(items, item.id, depth + 1, max_depth)
      end
      content += '</li>'
    end
    content += '</ul>'
    return content.html_safe
  end

  def sp_list_page(page, collection, depth, max_depth, options = {})
    active_class = 'menu-active'
    active_class = options[:active_class] if options.key?(:active_class)
    return '' if collection[page.id].blank?
    content = '<ul>'
    collection[page.id].sort_by(&:page_order).each do |page|
      active = false
      unless page.url_name.blank?
        if current_page?(page_path(id: page.url_name))
          active = true
        elsif page.url_name == Spud::Cms.root_page_name && current_page?(root_path)
          active = true
        end
      end

      content += "<li class='#{active_class if active}'><a href='#{page_path(id: page.url_name)}'>#{page.name}</a>"
      if max_depth == 0 || max_depth > depth
        content += sp_list_page(page, collection, depth + 1, max_depth, options)
      end
      content += '</li>'
    end
    content += '</ul>'
    return content.html_safe
  end

  def sp_list_menu_subnav(options = {})
    return_string = ''
    base_path = request.path[1..-1]
    menu_name = 'navigation'
    classes = 'subnav'

    menu_name = options[:name] if options.key?(:name)
    classes = options[:class] if options.key?(:class)

    menu = SpudMenu.where(name: menu_name).first

    unless menu.blank?
      menu_item = menu.spud_menu_items_combined
                      .joins("LEFT JOIN #{SpudPage.table_name} ON (#{SpudPage.table_name}.id = #{SpudMenuItem.table_name}.spud_page_id)")
                      .where([
                               "#{SpudPage.table_name}.url_name = :base_path OR #{SpudMenuItem.table_name}.url = :base_path_slash",
                               { base_path: base_path, base_path_slash: "/#{base_path}" }
                             ]).first

      unless menu_item.blank?
        begin
          if menu_item.spud_menu_items.count > 0
            return_string = sp_list_menu(class: classes, name: menu_name, start_menu_item_id: menu_item.id)
          else
            return_string = sp_list_menu(class: classes, name: menu_name, start_menu_item_id: menu_item.parent_id)
          end
        rescue
          return_string = ''
        end
      end
    end
    return return_string
  end

  def layout_options
    layouts = Spud::Cms::Engine.template_parser.layouts
    layout_options = []
    layouts.each_pair do |key, value|
      layout_options << [value[:template_name], key]
    end
    return layout_options
  end

end
