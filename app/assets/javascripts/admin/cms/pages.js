tb.cms.pages = {};

(function(){

tb.cms.pages = {
  edit: function(){
    tb.cms.pages.initFormTabs();
    $(".btn-preview").on('click', clickedPreviewButton);
    $("#spud_page_layout").on('change', changedLayoutSelection);
  }
};

var clickedPreviewButton = function(e){
  var button = $(this);
  var previewInput = $('input#preview');
  var form = button.parents('form')[0];

  button.button('reset');
  form.target = '_blank';
  previewInput.val(1);

  setTimeout(function(){
    form.target = '';
    form.onSubmit = null;
    previewInput.val(0);
    form.querySelectorAll('.btn').forEach(function(btn) {
      btn.disabled = false;
    });
  }, 200);
};

var changedLayoutSelection = function(e){
  var $this = $(this);
  $.get($this.attr("data-source"), { template: $this.val() }, function(data) {

    spud.admin.editor.unload();

    $("#page_partials_form").replaceWith(data);
    tb.cms.pages.initFormTabs();

    tb.editor.init();

  }, 'text').error(function(jqXHR) {
    alert("Error: " + jqXHR.responseText);
  });
};

tb.cms.pages.initFormTabs = function(){
  var tabNames = [];

  $('.formtabs .formtab').each(function(tabind) {
    if(tabind === 0){
      $(this).addClass('active');
    }
    this.id = 'tab-' + tabind;
    tabNames.push($('.tab_name',this).first().val());
  });

  var tabButtons = $('.formtabs .formtab_buttons').first();
  for(var x = 0; x < tabNames.length; x++){
    var tabButton = $('<li><a href="#tab-' + x + '" data-toggle="tab">' + tabNames[x] + '</a></li>');
    if(x === 0) {
      tabButton.addClass('active');
    }
    tabButtons.append(tabButton);
  }
};

})();
