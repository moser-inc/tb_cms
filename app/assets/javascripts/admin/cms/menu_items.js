tb.cms.menu_items = {};

(function() {
  var menu_items = tb.cms.menu_items;
  var val;

  var setUrl = function(){
    return '/admin/menus/'+ val +'/menu_items/update_sort';
  };

  var fixHelper = function(e, ui) {
    ui.children().each(function() {
      $(this).width($(this).width());
    });
    return ui;
  };

  menu_items.updateStatus = function(delay, shadow) {
    if(!delay) { //Default 300ms delay if not provided
      delay = 300;
    }
    setTimeout(function() {
      $('.detail-wrapper').css({
        boxShadow: shadow
      });
    }, delay);
  };

  var sortableIcons = function() {
    $("#sort tbody").sortable({
      update : function(e, ui) {
        var sortArr = [];
        $("#sort tbody > tr > input").each(function() {
          sortArr.push($(this).attr('value'));
        });
        val = $("#menu-id").val();
        //Update order of menuItems
        $.ajax(setUrl(), {
          method: 'PUT',
          data: {
            order: sortArr
          },
          dataType: "json",
          success: function(data, status, jqXHR) {
            menu_items.updateStatus(200, '0px 0px 26px 2px rgba(4, 252, -49, 0.59)');
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            menu_items.updateStatus(100, '0px 0px 26px 2px rgba(255, 0, 40, 0.59)');
            alert("Status: " + textStatus); alert("Error: " + errorThrown);
          }
        });
      }
    });
  };

  menu_items.init = function() {
    return

    // Sortable relies on jQuery UI, which has been removed
    // This code needs to migrate to sortable.js
    // See: https://bitbucket.org/moser-inc/tb_core/commits/e6252b6aa508e103b54df7e563ecfa2d2ec26c24
    //
    $("#sort tbody").sortable({
      helper: fixHelper,
      cursor: "move",
      activate: function( event, ui ) {
        menu_items.updateStatus(100, '0px 0px 26px 2px rgba(4, 252, 255, 0.59)');
      }
    });
    sortableIcons();
  };

})();
