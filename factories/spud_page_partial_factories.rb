FactoryBot.define do
  sequence :part_name do |n|
    "Part#{n}"
  end
  factory :spud_page_partial do
    name { FactoryBot.generate(:part_name) }
  end
end
