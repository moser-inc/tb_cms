FactoryBot.define do
  sequence :menu_name do |n|
    "Menu#{n}"
  end
  factory :spud_menu do
    name { FactoryBot.generate(:menu_name) }
  end
end
