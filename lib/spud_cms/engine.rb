require 'tb_core'
require 'tb_redirects'

module Spud
  module Cms
    class Engine < Rails::Engine
      engine_name :tb_cms

      config.generators do |g|
        g.test_framework :rspec, view_specs: false
      end

      initializer :admin do
        TbCore.configure do |config|
          config.admin_applications += [{ name: 'Pages', thumbnail: 'admin/pages_thumb.png', url: '/admin/pages', order: 0 }]
          if Spud::Cms.menus_enabled
            config.admin_applications += [{ name: 'Menus', thumbnail: 'admin/menus_thumb.png', url: '/admin/menus', order: 2 }]
          end

          if Spud::Cms.snippets_enabled
            config.admin_applications += [{ name: 'Snippets', thumbnail: 'admin/snippets_thumb.png', url: '/admin/snippets', order: 3 }]
          end
        end
      end

      initializer :spud_cms_routes do |config|
        config.routes_reloader.paths << File.expand_path('../page_route.rb', __FILE__)
      end

      initializer 'tb_cms.assets' do |_config|
        TbCore.append_admin_javascripts('admin/cms/application')
        TbCore.append_admin_stylesheets('admin/cms/application')
        Rails.application.config.assets.precompile += ['admin/pages_thumb.png', 'admin/snippets_thumb.png', 'admin/menus_thumb.png']
      end

      initializer :template_parser do |_config|
        @template_parser = Spud::Cms::TemplateParser.new
      end

      def template_parser
        return @template_parser
      end

    end
  end
end
