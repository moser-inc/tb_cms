require 'rails/generators/migration'

class Spud::Cms::LayoutGenerator < ::Rails::Generators::Base
  desc 'This generator creates a new spud cms layout file'
  argument :template_name, type: :string
  argument :attributes, type: :array, default: [], banner: 'content_block content_block'

  source_root File.expand_path('../templates', __FILE__)

  def create_layout
    template 'layout.html.erb', "app/views/layouts/#{template_name.downcase.underscore}.html.erb"
  end

  def create_layout_action
    concern_path = 'app/controllers/concerns/spud_cms_layout_actions.rb'
    unless File.exist?(File.join(Rails.root, concern_path))
      template 'layout_actions.rb', 'app/controllers/concerns/spud_cms_layout_actions.rb'
    end
    inject_into_file concern_path, after: "extend ActiveSupport::Concern\n" do
      <<-HEREDOC

  def #{template_name.downcase.underscore}_action(method)
    # this action is called when the #{template_name} cms layout is used
  end
HEREDOC
    end
  end

  private

  def concern_content
  end

end
