class AddTitleTagToSpudPage < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_pages, :page_title, :string
  end
end
