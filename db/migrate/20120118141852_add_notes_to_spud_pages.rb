class AddNotesToSpudPages < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_pages, :notes, :text
  end
end
