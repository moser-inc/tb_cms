class AddSymbolNameToSpudPagePartials < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_page_partials, :symbol_name, :string
  end
end
