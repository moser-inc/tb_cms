class AddSiteIdToSpudMenus < ActiveRecord::Migration[4.2]
  def change
  	add_column :spud_menus, :site_id, :integer
    add_index :spud_menus,:site_id
  end
end
