class RemoveSiteIdFromCmsTables < ActiveRecord::Migration[4.2]
  def up
    remove_column :spud_menus, :site_id
    remove_column :spud_pages, :site_id
    remove_column :spud_snippets, :site_id
  end
  def down
    add_column :spud_menus, :site_id, :integer
    add_column :spud_pages, :site_id, :integer
    add_column :spud_snippets, :site_id, :integer
  end
end
