$LOAD_PATH.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'spud_cms/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'tb_cms'
  s.version     = Spud::Cms::VERSION
  s.authors     = ['Moser Consulting']
  s.email       = ['greg.woods@moserit.com']
  s.homepage    = 'http://bitbucket.org/moser-inc/tb_cms'
  s.summary     = 'Twice Baked CMS'
  s.description = 'Content management engine for Twice Baked'

  s.files = Dir['{app,config,db,lib}/**/*'] + ['MIT-LICENSE', 'Rakefile', 'README.md']
  s.test_files = Dir.glob('spec/**/*').reject { |f| f.match(%{^spec\/dummy\/(log|tmp)}) }

  s.add_dependency 'rails', '>= 5.1.3'
  s.add_dependency 'tb_core', '>= 1.4.4'
  s.add_dependency 'tb_redirects', '>= 1.0.beta1'

  s.add_development_dependency 'pg', '~> 0.18'
  s.add_development_dependency 'rspec-rails', '>= 3.5.0'
  s.add_development_dependency 'rails-controller-testing'
  s.add_development_dependency 'factory_bot_rails'
  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'simplecov'
  s.add_development_dependency 'shoulda'
end
