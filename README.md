# TB CMS

TB CMS is a content managment engine intended for use with Rails and [Twice Baked](https://bitbucket.org/westlakedesign/tb_core/).

## Installation

First, it is recommended you run the steps listed in the "Installation/Usage" section of the [TB Core](https://bitbucket.org/westlakedesign/tb_core/) README file. Then, perform the following steps:

1. Add the following to your Gemfile

        gem 'tb_cms'

2. Run bundle install
3. Copy in database migrations to your new rails project

        bundle exec rake railties:install:migrations
        rake db:migrate

4. Restart your application

## Configuration

TB CMS accepts the following configuration options.

    Spud::Cms.configure do |config|
      config.menus_enabled = true
    end

## Defining Layouts

By default, CMS pages will use your standard `application.html.erb` layout. Chances are you will eventually want to specify different layouts for different pages. This is where page layouts come in handy.

A page layout is essentially a plain old Rails layout file with a little bit of metadata inserted at the top. The following example defines a layout with two content blocks, Left and Right.

    <%
    #template_name: Two Column
    #html: Left
    #html: Right
    %>
    <%= content_for :body do %>
      <div class="left">
        <%= yield :left %>
      </div>
      <div class="right">
        <%= yield :right %>
      </div>
    <% end %>
    <%= render :template => "/layouts/application" %>

You may also use the `spud:cms:layout` generator to quickly generate a layout file.

    rails g spud:cms:layout two_column left right

## Layout Actions

Layout actions provide a mechanism for running custom Ruby code when a given layout is hit. For example, suppose you have an `About` layout that needs to show an array of `Employee` records. Let's also imagine you want to show a different list of employees for `get` and `post` requests.

Create a controller concern at `app/controllers/concerns/spud_cms_layout_actions.rb` with the following code.

    module SpudCmsLayoutActions
      extend ActiveSupport::Concern

      def about_action(method)
        if method == :post
          @employees = Employee.where(:secret_agent => true).order('first_name asc')
        else
          @employees = Employee.where(:is_cool => true).order('first_name asc')
        end  
      end

    end

If you ran the `spud:cms:layout` generator described earlier, the concern and empty action would have been created for you automatically.

## Using Menus

A lot of cms engines allow you to render your navigation links in a ul block by using your page tree structure. In a lot of cases this is insufficient as some sites have urls that redirect to pages outside of your cms. This is where menus come in. They can be built in the admin control panel.
In your application layout file or any erb template you can render a ul block like so

    <%= sp_list_menu({:id => "navigation",:name => "Main"}) %>

This will output a <ul id="navigation"></ul> block for the menu you created in admin named "Main"

## Testing

TB CMS uses RSpec for testing. Get the tests running with a few short commands:

1. Create and migrate the databases:

        rake db:create
        rake db:migrate

2. Load the schema in to the test database:

        rake app:db:test:prepare

3. Run the tests with RSpec

        rspec spec

After the tests have completed the current code coverage stats is available by opening ```/coverage/index.html``` in a browser.
